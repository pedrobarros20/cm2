import { Component, OnInit } from '@angular/core';
import { MenuCtrlService } from '../../services/menu-ctrl.service';

@Component({
  selector: 'app-mobile-ribbon',
  templateUrl: './mobile-ribbon.component.html',
  styleUrls: ['./mobile-ribbon.component.scss']
})
export class MobileRibbonComponent implements OnInit {

  constructor(
    private menuCtrlService: MenuCtrlService
  ) { }

  openMenu() {
    this.menuCtrlService.open();
  }

  ngOnInit() {
  }

}
