import { Injectable } from '@angular/core';
// import { ErrorDialogService } from '../error-dialog/errordialog.service';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse,
    HttpParams
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/modules/login/services/auth/auth.service';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService {

  constructor(
    private authService: AuthService,
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.includes(environment.server.API) && !request.url.includes(environment.server.LOGIN)) {
        const token: string = this.authService.getUser().token;
        request = request.clone({
          // headers: request.headers.set('Authorization', 'Bearer ' + token),
          params: new HttpParams().set('auth', token
        )});
        // request.params.append('access_token', token);
        console.log(request.params);
    }

    if (!request.headers.has('Content-Type')) {
        request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
          // if (event instanceof HttpResponse) {
          //     console.log('event--->>>', event);
          // }
          return event;
      }),
      catchError((error: HttpErrorResponse) => {
        let data = {};
        data = {
            reason: error && error.error.reason ? error.error.reason : '',
            status: error.status
        };
        if (error.status === 401 || error.status === 403) {
          if (error.url.includes(environment.server.LOGIN)) {
            // Don't Logout
            // return throwError(error);
            this.translateService.get('USER_PASS_UNMATCH').subscribe(message => this.toastrService.error(message));
          } else {
            this.toastrService.error(error.message);
            this.authService.logout('/login');
          }
          return;
        }
        this.toastrService.error(error.message);
        return throwError(error);
      })
    );
  }
}
