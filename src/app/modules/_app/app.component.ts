import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TranslateConfigService } from './services/translateConfig/translate-config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(
    private translateConfigService: TranslateConfigService
  ) {
    // Init Translate
    this.translateConfigService.init();
  }
}
