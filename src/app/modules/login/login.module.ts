import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { AuthService } from './services/auth/auth.service';
import { SharedModule } from '../_shared/_shared.module';

@NgModule({
  declarations: [LoginComponent, SignInComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    SharedModule
  ],
  bootstrap: [LoginComponent]
})
export class LoginModule {
}
