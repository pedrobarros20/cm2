import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XreviveComponent } from './xrevive.component';

describe('XreviveComponent', () => {
  let component: XreviveComponent;
  let fixture: ComponentFixture<XreviveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XreviveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XreviveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
