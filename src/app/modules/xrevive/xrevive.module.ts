import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { XreviveRoutingModule } from './xrevive-routing.module';
import { XreviveComponent } from './xrevive.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../_shared/_shared.module';
import { UiFormModule } from '../_ui-form/_ui-form.module';

@NgModule({
  declarations: [XreviveComponent, DashboardComponent],
  imports: [
    CommonModule,
    XreviveRoutingModule,
    ChartsModule,
    SharedModule,
    UiFormModule
  ]
})
export class XreviveModule { }
