import { Component, OnInit } from '@angular/core';
import { InputConfig } from '../../_shared/components/search-header/search-header.component';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import * as _ from 'lodash';
import { titles, propertiesOptions, type, dataSetConstructor, linesProperties } from '../charts-generators.ts/graphic-generator';
import * as spotifyData from './mocks/spotify-chart.json';
import * as kkboxData from './mocks/kkbox-chart.json';
import * as analysisData from './mocks/analysis-chart.json';
import * as channelUsageData from './mocks/channel-usage-chart.json';
import * as inputData from './mocks/input-channel-usage.json';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public inputs: InputConfig[] = inputData.data
  public titlesBarChart = ['Asia', 'Europe', 'North America', 'Oceania', 'South America'];

  // CHANNEL USAGE CHART

  public barChartTitlesChannelUsage = this.titlesBarChart;
  public channelUsageObject = channelUsageData.data
  public barChartOptions: ChartOptions = new propertiesOptions().options;
  public barChartLabels: Label[] = new titles(this.barChartTitlesChannelUsage).labels;
  public barChartType: ChartType = new type('bar').type;
  public barChartData: ChartDataSets[] = new dataSetConstructor(this.channelUsageObject).data
  public blacklistChannelUsage: Set<ChartDataSets> = new Set();


  // ANALYSIS CHART

  public barChartTitlesAnalysis = this.titlesBarChart;
  public analysisObject = analysisData.data
  public barChartOptionsAnalysis: ChartOptions = new propertiesOptions().options;
  public barChartLabelsAnalysis: Label[] = new titles(this.barChartTitlesAnalysis).labels;
  public barChartTypeAnalysis: ChartType = new type('bar').type;
  public barChartDataAnalysis: ChartDataSets[] = new dataSetConstructor(this.analysisObject).data;
  public blacklistAnalysisChart: Set<ChartDataSets> = new Set();
  

  // SPOTIFY CHART

  public labelsMusicCharts: Label[] = ['Morning', 'Afternoon', 'Night', 'Dawn'];
  public spotifyChartObject = spotifyData.data;
  public lineChartLabelsSpotify: Label[] = new titles(this.labelsMusicCharts).labels;
  public lineChartDataSpotify: ChartDataSets[] = new dataSetConstructor(this.spotifyChartObject).data;
  public lineChartLegendSpotify = false;
  public lineChartOptionsSpotify: ChartOptions = new propertiesOptions().options;
  public lineChartTypeSpotify: ChartType = new type('line').type;
  public lineChartPluginsSpotify = [];
  public lineChartColorsSpotify: Color[] =  new linesProperties().colors;
  public blacklistSpotify: Set<ChartDataSets> = new Set();


  // KKBOX CHART

  public kkboxChartObject = kkboxData.data;
  public lineChartOptionsKkbox: ChartOptions = new propertiesOptions().options;
  public lineChartLabelsKkbox: Label[] = new titles(this.labelsMusicCharts).labels;
  public lineChartDataKkbox: ChartDataSets[] = new dataSetConstructor(this.kkboxChartObject).data;
  public lineChartLegendKkbox = true;
  public lineChartTypeKkbox: ChartType = new type('line').type;
  public lineChartPluginsKkbox = [];
  public blacklistKkbox: Set<ChartDataSets> = new Set();

  // RECEIVE OUTPUTS

  receiveLegend(clickLegend, blacklist: Set<ChartDataSets>) {

    if(blacklist == this.blacklistKkbox){
      var labelsSpotify = this.lineChartDataSpotify.map(({ label }) => label);
      // Array de labels que o gráfico Spotify possui

      var labelSelected = clickLegend.label;
      // Label selecionada do KKbox
      
      for (let i in labelsSpotify) {
        if (labelsSpotify[i] === labelSelected) {
          // Se a label selecionada for a mesma que a do Spotify
          this.blacklistSpotify.has(this.lineChartDataSpotify[i]) ? this.blacklistSpotify.delete(this.lineChartDataSpotify[i]) :  this.blacklistSpotify.add(this.lineChartDataSpotify[i]);
          // Coloco, removo e verifico na blacklist o data daquela label 
        }
      }
      blacklist.has(clickLegend) ? blacklist.delete(clickLegend) : blacklist.add(clickLegend);
    }else {
      blacklist.has(clickLegend) ? blacklist.delete(clickLegend) : blacklist.add(clickLegend);
    }
  }

  receiveForm(clickSearch) {
    console.log(clickSearch);
  }

  constructor() {
  }

  ngOnInit() {

  }

}
