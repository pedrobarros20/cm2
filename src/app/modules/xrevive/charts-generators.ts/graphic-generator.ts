import { ChartDataSets, ChartOptions, ChartType, Chart } from 'chart.js';
import { Label } from 'ng2-charts';

export class titles {

    public labels: Label[] = null;

    constructor(titles: Label[] ) {
        this.setData(titles);
    }

    setData(titles: Label[]){
       this.labels = titles;
    }
  
}

export class barsProperties {

    public colors: any = [
        {
          backgroundColor: '#bafd96',
          borderColor: '#a0d981',
          borderWidth: 1.5,
        },
        { 
          backgroundColor: '#96cdfd',
          borderColor: '#81b0d9',
          borderWidth: 1.5,
        }];
    
    constructor() {
    }
  
}

export class propertiesOptions {

    public options: ChartOptions = {
        legend: {
          display: false,
        },
        responsive: true,
        scales: {
          xAxes: [{
              barThickness: 25,
              gridLines: {
                display: false,
                zeroLineColor: 'red',
              },
              ticks: {
                fontColor: '#7E7E7F',
                beginAtZero: true,
            },
          }],
          yAxes: [{
            gridLines: {
              drawBorder: false,
              color: '#e9e9eb',
            },
            ticks: {
                fontColor: '#bcbcbc',
                fontSize: 11,
                padding: 10,
                min: 0,
                max: 30,
                stepSize: 5,
                callback: function(label, index, labels) {
                  return label+'k';
                }
            },
            scaleLabel: {
              display: false,
          }
        }],
      },
    
    };
    
    constructor() {
    }
  
}

export class type {

    public type: ChartType = null;
    
    constructor(type: ChartType) {
        this.setData(type);
    }

    setData(type: ChartType){
        this.type = type;
    }
  
}

export class dataSetConstructor {

    public data: ChartDataSets[] = null;
    
    constructor(object: ChartDataSets[]) {
        this.setData(object);
    }

    setData(object: ChartDataSets[]){
        this.data = object;
    }
  
}

export class linesProperties {

  public colors: any = [
        {
          borderColor: '#fe9696', 
          backgroundColor: '#f9a4a4',
          pointBackgroundColor: '#fe9asa696',
          borderWidth: 2,
          pointBorderWidth: 2,
          pointBorderColor: '#fff'
        },
        {
          borderColor: '#a692f6',
          backgroundColor: '#aa96fe',
          pointBackgroundColor: '#a692f6'
        },
        {
          borderColor: '#adcaff',
          backgroundColor: '#b3cefc',
          pointBackgroundColor: '#adcaff',
          borderWidth: 2
        },
        {
          borderColor: '#efb15d',
          backgroundColor: '#f4b561',
          pointBackgroundColor: '#efb15d',
          borderWidth: 2
        },
        {
          borderColor: '#98d477',
          backgroundColor: '#a4e57e',
          pointBackgroundColor: '#98d477',
          borderWidth: 2
        },
        {
          borderColor: '#707070',
          backgroundColor: '#606060',
          pointBackgroundColor: '#707070',
          borderWidth: 2
        }
      ];
  
  constructor() {
  }

}
