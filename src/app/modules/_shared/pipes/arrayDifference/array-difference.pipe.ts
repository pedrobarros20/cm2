import { Pipe, PipeTransform } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import * as _ from 'lodash';

@Pipe({
  name: 'arrayDifference',
  pure: false
})
export class ArrayDifferencePipe implements PipeTransform {

  transform(originalList: Array<ChartDataSets>, blacklist: Set<ChartDataSets>): Array<ChartDataSets> {
    return  _.difference(originalList, Array.from(blacklist));
  }

}
