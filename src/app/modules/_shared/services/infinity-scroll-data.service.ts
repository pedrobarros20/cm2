import { Injectable, Inject, Component, ComponentDecorator, ComponentRef } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';

const STATES = {
  IDLE: 'idle',
  LOADING: 'loading',
  FULL: 'full'
};

export class INITCONFIG {
  public httpClient: HttpClient = null;
  public path: string =  null;
  public pageSize ? = 20;
  public scrollElement: HTMLElement = null;
  public getDataScrollPositon ? = 80;
  public page ? = 0;
  public params?: {} = {};
  readonly cbList ? = [];
  readonly beforeCbList ? = [];
  readonly getRequestSub?: Subscription = null;
}

@Injectable()
export class InfinityScrollDataService {
  private httpClient: HttpClient;
  private path: string;
  private pageSize = 20;
  private scrollElement: HTMLElement;
  private getDataScrollPositon = 80;
  private params?: {} = {};

  protected scrollGetDataEv: any;
  protected getRequestSub: Subscription;

  // State
  private state = STATES.IDLE;

  // Inits
  private page = 0;

  private cbList = [];
  private beforeCbList = [];

  constructor(
    // @Inject('path') private path: string,
    // @Inject('httpClient') private http: HttpClient

  ) {
    this.reset();
  }
  public config(config: INITCONFIG) {
    this.reset();
    Object.assign(this, config);
    this.init();
    return this;
  }

  private init() {
    // init;
    this.bindScroll();
    // this.state = STATES.LOADING;
    // this.getData();
  }

  private getData($event: Event, ignoreScroll: boolean) {
    if ( ignoreScroll || ( this.state !== STATES.LOADING && this.checkScrollPosition()) ) {
      this.execBeforeGetDataCallBacks();
      this.state = STATES.LOADING;
      this.updatePageTo(this.page + 1);

      this.getRequestSub = this.httpClient.get(this.path, { params: this.createParams() }).subscribe( (res: any[]) => {
        // console.log('pegou');
        if ( res.length < this.pageSize) {
          this.state = STATES.FULL;
          this.unBindScroll();
        } else {
          this.state = STATES.IDLE;
        }
        const status = {
          page: this.page,
        };
        // this.unBindScroll();
        this.execCallBacks(res, status);
      });
    }
  }

  private bindScroll() {
    this.scrollGetDataEv = this.getData.bind(this);
    this.scrollElement.addEventListener('scroll', this.scrollGetDataEv, true);
    // this.scrollElement.removeEventListener('scroll', this.getData, false );
  }

  private unBindScroll() {
    this.scrollElement.removeEventListener('scroll', this.scrollGetDataEv, true );
  }

  private updatePageTo(pageNumber: number) {
    this.page = pageNumber;
  }

  private createParams() {
    const params = {};
    for (const key in this.params) {
      if (this.params.hasOwnProperty(key)) {
        params[key] = this.params[key];
      }
    }
    Object.assign(params, {page:  this.page.toString(10), pageSize: this.pageSize.toString() });
    // this.params.forEach( e => params.set(e.header, e.value));
    // params.append('page', this.page.toString(10)).append('pageSize', this.pageSize.toString());
    // params = new HttpParams().set('page', this.page.toString(10)).set('pageSize', this.pageSize.toString());
    console.log(params);
    // return new HttpParams().set('page', this.page.toString(10)).set('pageSize', this.pageSize.toString());
    return params;
  }

  private registerCallBacksTo(list, cb) {
    list.push(cb);
  }

  private execCallBacks(res, status) {
    this.cbList.forEach(e => e(res, status));
  }

  private execBeforeGetDataCallBacks() {
    this.beforeCbList.forEach(e => e());
  }

  private checkScrollPosition() {
    const el = this.scrollElement;
    const height = el.clientHeight;
    const scrolable = el.scrollHeight - height;
    const scrollPostion = el.scrollTop;
    const percent = scrollPostion * 100 / scrolable;
    // console.log(percent);
    return percent >= this.getDataScrollPositon;
  }

  private reset() {
    // tslint:disable-next-line:no-unused-expression
    this.getRequestSub && this.getRequestSub.unsubscribe();
    // tslint:disable-next-line:no-unused-expression
    this.scrollElement && this.unBindScroll();
    Object.assign(this, new INITCONFIG());
  }

  public destroy() {
    this.reset();
  }

  public setParams(params: {}) {
    this.params = params;
  }

  public setPath(path: string) {
    this.path = path;
  }

  public beforeGetData(callBack) {
    if ( callBack && typeof callBack === 'function') {
      this.registerCallBacksTo(this.beforeCbList, callBack);
    }
  }

  public onGetData(callBack: (res?, status?) => any) {
    if ( callBack && typeof callBack === 'function') {
      this.registerCallBacksTo( this.cbList, callBack);
    }
  }

  public start() {
    this.getData(null, true);
  }

  public clear() {
    this.page = 0;
    return this;
  }
}
