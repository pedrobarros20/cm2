import { TestBed } from '@angular/core/testing';

import { InfinityScrollDataService } from './infinity-scroll-data.service';

describe('InfinityScrollDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InfinityScrollDataService = TestBed.get(InfinityScrollDataService);
    expect(service).toBeTruthy();
  });
});
