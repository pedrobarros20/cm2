import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';

export class HeaderEvent {

  constructor(
    public header: string,
    public order: 'ascending' | 'descending'
  ) {
  }
}

@Component({
  selector: 'app-ui-table',
  templateUrl: './ui-table.component.html',
  styleUrls: ['./ui-table.component.scss']
})
export class UiTableComponent implements OnInit, OnChanges {

  @Input() data: any[];
  @Input() headers: string[];
  @Input() allowReorder = true;
  @Input() customViews: any = {};
  @Input() noDataTitle = 'No data';
  @Input() loading: boolean | Subscription = false;

  @Input() activeHeader: HeaderEvent = new HeaderEvent(null, 'ascending');

  @Output() clickHeader = new EventEmitter<HeaderEvent>();
  @Output() clickRow = new EventEmitter();
  @Output() clickCell = new EventEmitter();
  // @Output() beforeReorder = new EventEmitter();

  constructor() { }

  ngOnInit() {

  }

  setActiveHeader(header: string) {
    if (this.activeHeader.header === header) {
      this.activeHeader.order = this.activeHeader.order === 'ascending' ? 'descending' : 'ascending';
    } else if (this.allowReorder) {
      this.activeHeader = new HeaderEvent(header, 'ascending');
    }
  }

  emitHeader(headerKey: string, order: 'ascending' | 'descending' ) {
    this.clickHeader.emit(new HeaderEvent(headerKey, order));
  }

  emitRow(row: any) {
    this.clickRow.emit(row);
  }

  emitCell(cell: any) {
    this.clickCell.emit(cell);
  }

  clickOnHeader(headerKey) {
    this.setActiveHeader(headerKey);
    this.emitHeader(headerKey, this.activeHeader.order);
  }

  prepareLoading() {
    // if ( typeof this.loading === 'object' && this.loading.isPrototypeOf(Subscription)) {
    //    this.loading
    // }
  }

  ngOnChanges(changes: SimpleChanges) {
  }

}

