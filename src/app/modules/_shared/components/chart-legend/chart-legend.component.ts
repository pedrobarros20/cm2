import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChartDataSets } from 'chart.js';

@Component({
  selector: 'app-chart-legend',
  templateUrl: './chart-legend.component.html',
  styleUrls: ['./chart-legend.component.scss']
})
export class ChartLegendComponent implements OnInit {

  @Input() data: ChartDataSets[];
  @Output() clickLegend = new EventEmitter();

  emitLegend(i: ChartDataSets) {
      this.clickLegend.emit(i);
  }
  
  constructor() { }

  ngOnInit() {

  }

}
