import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() iconPath: string;
  @Input() buttonText: string;

  @Input() lightButton: boolean | '' = false;
  @HostBinding('class.light-button')
  public get isLightButton(): boolean {
    return this.lightButton === '' ? true : this.lightButton;
  }

  constructor() {

  }

  ngOnInit() {
  }

}
