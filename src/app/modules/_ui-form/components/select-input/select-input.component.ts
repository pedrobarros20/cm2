import { Component, OnInit, AfterViewChecked, Input, Optional, Inject, forwardRef, HostListener, AfterContentChecked } from '@angular/core';
import { InputBase } from '../../inputBase';
import { NG_VALIDATORS, NG_ASYNC_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TextInputComponent } from '../text-input/text-input.component';

export class SelectItemInterface {
  value: string;
  label: string;
}

@Component({
  selector: 'app-select-input',
  templateUrl: './select-input.component.html',
  styleUrls: ['./select-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectInputComponent),
    multi: true
  }]
})
export class SelectInputComponent extends InputBase<string> implements OnInit, AfterViewChecked, AfterContentChecked {

  @Input() selectOptions: SelectItemInterface[] = [];

  public displayOptions = false;

  public get labelOnDisplay(): string {
    const selectItem =  this.selectOptions.find( e => e.value === this.value);
    const label = selectItem ? selectItem.label : null;
    // if ( !label) {
    //   this.value = null;
    // }
    return selectItem ? selectItem.label : null;
  }

  protected mouseEv = ($event: Event) => {};

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
    // console.log(this.value);
  }

  public setDisplayOptions(bool: boolean, $event) {

    this.displayOptions = bool;
        // tslint:disable-next-line:no-unused-expression
    bool && this.bindMouseEv($event);
  }

  public selectItem(item: SelectItemInterface) {
    this.value = item.value;
  }

  private bindMouseEv(event: Event) {
    // tslint:disable-next-line:max-line-length
    // event.stopPropagation();
    this.mouseEv = ($event) => {
      // this.setDisplayOptions.bind(this)(false);
      const target = ($event.target as HTMLElement);
      if (target.className && target.className.indexOf('select-option') >= 0) {
        target.click();
      }
      document.removeEventListener('click', this.mouseEv, true);
      this.setDisplayOptions.bind(this)(false);
    };
    // event.stopPropagation();
    event.preventDefault();
    document.addEventListener('click', this.mouseEv, true);

  }
  ngAfterContentChecked() {
    const item = this.selectOptions.find( e => e.value === this.value);
    const value  = item ? item.value : null;
    if (value !== this.value) {
      this.value = value;
    }
  }
  ngOnInit() {
    // console.log(this.darkMode);
    // setInterval( () => {
    //   console.log(this.value);
    // }, 1000);
  }

  ngAfterViewChecked() {
    // super.init()

  }
}
