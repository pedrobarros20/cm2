import {
  Component,
  Optional,
  Inject,
  Input,
  ViewChild,
  OnInit,
  forwardRef,
  AfterViewInit,
  AfterViewChecked,
  HostBinding,
} from '@angular/core';

import {
  NgModel,
  NG_VALUE_ACCESSOR,
  NG_VALIDATORS,
  NG_ASYNC_VALIDATORS,
} from '@angular/forms';
import { InputBase } from '../../inputBase';

// import {ElementBase, animations} from '../form';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html',
  styleUrls: ['./text-input.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TextInputComponent),
    multi: true
  }],
})
export class TextInputComponent extends InputBase<string> implements OnInit, AfterViewChecked {

  @Input() pattern: string;

  constructor(
    @Optional() @Inject(NG_VALIDATORS) validators: Array<any>,
    @Optional() @Inject(NG_ASYNC_VALIDATORS) asyncValidators: Array<any>,
  ) {
    super(validators, asyncValidators);
  }
  ngOnInit() {
    // console.log(this.darkMode);
    // setInterval( () => {
    //   console.log(this.darkMode);
    // }, 1000);
  }

  ngAfterViewChecked() {
    // super.init()

  }
}
