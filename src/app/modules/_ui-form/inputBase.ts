import {NgModel} from '@angular/forms';

import {ValueAccessorBase} from './valueAcessorBase';

import {
  AsyncValidatorArray,
  ValidatorArray,
} from './validate';
import { Input, ViewChild, HostBinding } from '@angular/core';
import { SelectItemInterface } from './components/select-input/select-input.component';

export interface InputBaseInterface {
  placeholder: string;
  selectOptions?: SelectItemInterface[];
//   model: NgModel;
//   filled?: boolean;
//   focus?: boolean;
//   required?: boolean;
}

export abstract class InputBase<T> extends ValueAccessorBase<T> {

  // InputBaseInterface
  @Input() public placeholder: string;
  @ViewChild(NgModel) model: NgModel;
  @Input() public required = false;
  public filled: boolean;
  public focus: boolean;
  // END InputBaseInterface

  @Input() public darkMode: boolean | '' = false;
  @HostBinding('class.dark-mode')
  public get isDarkMode(): boolean {
    return this.darkMode === '' ? true : this.darkMode;
  }

  constructor(
    private validators: ValidatorArray,
    private asyncValidators: AsyncValidatorArray,
  ) {
    super();
  }

  // protected validate(): Observable<ValidationResult> {
  //   const x = validate
  //   (this.validators, this.asyncValidators)
  //   (this.model.control);
  //   console.log(x);
  //   return x;
  // }

  // protected get invalid(): Observable<boolean> {
  //   // return this.model.invalid;
  //   return this.validate().pipe(map((v => Object.keys(v || {}).length > 0)));
  // }

  // protected get failures(): Observable<Array<string>> {
  //   return this.validate().pipe(map(v => Object.keys(v).map(k => message(v, k))));
  // }

  // private checkFilled(value): boolean {
  //   console.log(value);
  //   return value ? true : false;
  // }

  // changeValue($event) {
  //   this.filled = this.checkFilled($event);
  //   console.log(this.filled);
  // }

  init() {
    // this.checkFilled(this.value);
  }
}
