import {ControlValueAccessor} from '@angular/forms';

export abstract class ValueAccessorBase<T> implements ControlValueAccessor {
  private innerValue: T;

  private changed = new Array<(value: T) => void>();
  private touched = new Array<() => void>();

  public filled = false;
  public focused = false;

  constructor() {
    this.registerOnChange(() => {
      this.checkfilled();
    });
  }

  private checkfilled() {
    // tslint:disable-next-line:triple-equals

    // if ( typeof this.value === 'boolean' && this.value === false) {
    //   this.filled = true;
    // } else {
      this.filled = this.value !== undefined && this.value !== null && this.value as any !== '';
    // }

  }

  get value(): T {
    return this.innerValue;
  }

  set value(value: T) {
    console.log('setou');
    if (this.innerValue !== value) {
      this.innerValue = value;
      this.changed.forEach(f => f(value));
    }
  }

  writeValue(value: T) {
    // tslint:disable-next-line:triple-equals
    if (this.innerValue != value) {
      this.touch();
    }
    this.innerValue = value;
    this.checkfilled();
  }

  registerOnChange(fn: (value: T) => void) {
    // console.log('REGISTER CHANGE');
    this.changed.push(fn);
  }

  registerOnTouched(fn: () => void) {
    // console.log('REGISTER TOUCH');
    this.touched.push(fn);
  }

  touch() {
    // console.log(' TOUCH');
    this.touched.forEach(f => f());
  }

  setDisabledState?(isDisabled: boolean): void {
  }

}
