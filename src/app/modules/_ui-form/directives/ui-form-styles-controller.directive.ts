import { Directive, ElementRef, OnInit, Renderer2, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { InputBase } from '../inputBase';

@Directive({
  selector: '[appUiFormStylesController]'
})
export class UiFormStylesControllerDirective implements OnInit, OnChanges {

  @Input() public appUiFormStylesController: InputBase<any>;
  @Input() public appUiFormStylesControllerModelElement: HTMLElement;
  // @Input() public darkMode = false;

  // @Input() public invalid: Observable<boolean> = of(true);

  // @ViewChild('uiModel') uiModel: ElementRef;
  constructor(
    private elementRef: ElementRef,
    private renderer: Renderer2
    ) {
    }

  private bindFocus(el: HTMLElement) {
    el.addEventListener('focus', () => {
      this.appUiFormStylesController.focus = true;
    });
  }

  private bindBlur(el: HTMLElement) {
    el.addEventListener('blur', () => {
      this.appUiFormStylesController.touch();
      this.appUiFormStylesController.focus = false;
    });
  }

  // private toogleDarkMode(bool: boolean | '') {
  //   // this.appUiFormStylesController.darkMode = bool === '' || bool;
  //   bool === '' || bool ?
  //     this.renderer.addClass(this.elementRef.nativeElement, 'dark-mode')
  //   : this.renderer.removeClass(this.elementRef.nativeElement, 'dark-mode');
  // }


  ngOnInit(): void {
    this.bindFocus(this.appUiFormStylesControllerModelElement);
    this.bindBlur(this.appUiFormStylesControllerModelElement);
  }
  // ngAfterViewInit(): void {
  //   setInterval( () => console.log(this.invalid), 1000);
  // }

  ngOnChanges(simpleChanges: SimpleChanges) {
    // tslint:disable-next-line:no-unused-expression
    // simpleChanges.darkMode && this.toogleDarkMode(simpleChanges.darkMode.currentValue);
    // tslint:disable-next-line:max-line-length
    // setTimeout( () => this.toogleDarkMode(simpleChanges.appUiFormStylesController.currentValue.darkMode));
  }

}
