import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DevicesComponent } from './devices.component';
import { DevicesListComponent } from './pages/devices-list/devices-list.component';
import { DeviceViewComponent } from './pages/device-view/device-view.component';

const routes: Routes = [
  {
    path: '', component: DevicesComponent, children: [
      { path: '', component: DevicesListComponent },
      { path: ':id', component: DeviceViewComponent },
      { path: '**', redirectTo: '' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DevicesRoutingModule { }
