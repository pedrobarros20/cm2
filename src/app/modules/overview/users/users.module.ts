import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './pages/users-list/users-list.component';
import { UserViewComponent } from './pages/user-view/user-view.component';
import { SharedModule } from '../../_shared/_shared.module';
import { UiTablePictureComponent } from './pages/users-list/components/ui-table-picture/ui-table-picture.component';

@NgModule({
  declarations: [UsersComponent, UsersListComponent, UserViewComponent, UiTablePictureComponent ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule
  ],
  entryComponents: [ UiTablePictureComponent ]
})
export class UsersModule { }
